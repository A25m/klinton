from scapy.all import IP, TCP, sr, sr1


targets = ["127.0.0.1", "192.168.1.50"]
port_range = [22,993,443,8080] 

for target in targets:
	for port in port_range:
		# Создаем TCP пакет с флагом SYN для сканирования порта
		ip_packet = IP(dst=target)
		tcp_packet = TCP(dport=port, flags="S")
		
		# Отправляем пакет и получаем ответ
		print('Testing ip:',target, "port:", port)
		response = sr1(ip_packet / tcp_packet, timeout=3, verbose=0)
		
		# Обрабатываем полученный ответ
		if response:
			if response.haslayer(TCP) and response[TCP].flags == 18:  # TCP SYN-ACK
				print(f"{port} open")		
		
			
#2. Реализовать сканер открытых портов для нескольких целей.
#Указать несколько целей можно с помощью структуры типа список.
#Необходимо воспользоваться циклами.
#Используй конструкцию for с итерацией IP-адреса.